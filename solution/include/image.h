#ifndef ROTATE_H
#define ROTATE_H
#include <stdint.h>
#include <stdio.h>
struct pixel {
    uint8_t blue, green, red;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image rotate(struct image const* source );
void free_image(struct image* img);
#endif //ROTATE_H
