#include "image.h"
#include <stdlib.h>

struct image rotate(struct image const* source )
{
    struct image output={
        .width=source->height,
        .height=source->width,
        .data=malloc(sizeof(struct pixel)*source->width*source->height)
    };
    for (size_t i=0; i<source->width; i++)
    {
        for (size_t j=0; j<source->height; j++)
        {
            output.data[i*output.width+j]=(source->data[(source->height-1-j)*source->width+i]);
        }
    }
    return output;
}
void free_image(struct image* img)
{
    free(img->data);
    img->data=NULL;
    img->width=0;
    img->height=0;
}
