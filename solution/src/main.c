#include "../include/bmp.h"
#include "../include/image.h"
#include <stdio.h>

int main(int argc, char **argv)
{
    (void) argc;
    (void) argv; // supress 'unused parameters' warning

    struct image image={0};
    FILE *in=fopen(argv[1], "rb");
    from_bmp(in, &image);
    fclose(in);

    struct image image_rotated=rotate(&image);
    free_image(&image);

    FILE *out=fopen(argv[2], "wb");
    to_bmp(out, &image_rotated);
    fclose(out);
    free_image(&image_rotated);
    
    return 0;
}
