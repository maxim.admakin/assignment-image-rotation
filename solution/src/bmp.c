#include "bmp.h"

static struct bmp_header bmp_header_set(const struct image* img) 
{
    size_t pixel_size=sizeof(struct pixel);
    size_t bmp_header_size=sizeof(struct bmp_header);
    return (struct bmp_header){
           .bfType=0x4d42,
           .bfileSize=img->height*img->width*pixel_size+img->height*(img->width%4)+bmp_header_size,
           .bfReserved=0,
           .bOffBits=bmp_header_size,
           .biSize=40,
           .biWidth=img->width,
           .biHeight=img->height,
           .biPlanes=1,
           .biBitCount=24,
           .biCompression=0,
           .biSizeImage=img->height*img->width*pixel_size+(img->width%4)*img->height,
           .biXPelsPerMeter=0,
           .biYPelsPerMeter=0,
           .biClrUsed=0,
           .biClrImportant=0,
   };
}

enum read_status from_bmp(FILE* in, struct image* img) 
{
    struct bmp_header bmp_header={0};
    if(fread(&bmp_header, 1, sizeof(struct bmp_header), in) !=sizeof(struct bmp_header)) return READ_ERROR;
    img->height=bmp_header.biHeight;
    img->width=bmp_header.biWidth;
    img->data=(struct pixel*) malloc(img->width*img->height*sizeof(struct pixel));

    for (size_t i=0; i<img->height; i++)
    {
        if(fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in) != img->width 
                || fseek(in, (uint8_t)img->width %4, SEEK_CUR))
        {
            return READ_ERROR;
        } 
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img)
{
    struct bmp_header bmp_header=bmp_header_set(img);
    bmp_header.biHeight=img->height;
    bmp_header.biWidth=img->width;

    if(fwrite(&bmp_header, sizeof(struct bmp_header), 1, out)!=1) return WRITE_ERROR;
    const uint64_t zero=0;
    for(size_t i=0; i<img->height; i++)
    {
        if(fwrite(&(img->data[i*img->width]), sizeof(struct pixel), img->width, out) !=img->width 
                || fwrite(&zero, 1, img->width%4, out)!=img->width%4)
        {
            return WRITE_ERROR;
        }
          
    }
    return WRITE_OK;
}
